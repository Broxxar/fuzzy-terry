#Fuzzy Terry
-----------
A simple Fuzzy creature named Terry. His life is governed by fuzzy rules.

##Summary
Terry acts autonomously based on a set of fuzzy rules. Currently, rules can only be provided in code and there is no front end. A **FuzzyRule** is a tuple of a **FuzzyTerm** and a **String** that represents the action that the rule contributes to.

Consider the expression:

    if Stamina is not Energetic and Time is Nighttime then Sleep
    
In code this manifests as:
```cs
    new FuzzyRule(
            new FuzzyJoin(
                new FuzzyComplement (new FuzzyTerm("Stamina", "Energetic")),
                new FuzzyTerm("Time", "Nighttime"),
            "AND"),
        "Sleep")
```

Essentially, we create a new **FuzzyRule** object where we have two **FuzzyTerm**s joined by an **AND**. The first term is the Complement of Stamina is Energetic, or *"Stamina is not Energetic"*. The second term is simply *"Time is Nighttime"*. Then, we provide the **AND** operator to the **FuzzyJoin** and finally, provide the String that represents the desired action, in this case *"Sleep"*.

Every object inheriting from the abstract **FuzzyExpression** provides an Evaluate method which simply returns a number in the range [0, 1]. The **FuzzyController** class provides an interface for adding rules to the system, and for Evaluating all the rules to return an output set. Output sets describe which which actions had rules, how many rules pointed to any given action, and the total value of all the rules pointing to the same action.

These output sets can be used to derive which action Terry should take at any given time.

##Todo
Terry represent a simple and cut down version of what a typical NPC might do in a game. He performs short term decision makin, and doesn't do anything *too* interesting. Future work with Terry will have a visual front end for build rules, as well have long term goals to achieve (Terry would like to build a house with all that wood he collects).

##Builds
32 and 64 bit Linux builds are provided, along with a web player builds (the Unity Web Player plugin is available to Mac and PC). These can be found in the downloads section.

-------------
######Dan Moran 2013