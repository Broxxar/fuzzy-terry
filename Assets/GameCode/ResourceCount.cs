﻿using UnityEngine;
using System.Collections;

public class ResourceCount : MonoBehaviour {

	TextMesh fruit;
	TextMesh wood;

	void Awake () {
		fruit = transform.FindChild("fruitText").GetComponent<TextMesh>();
		wood = transform.FindChild("woodText").GetComponent<TextMesh>();
	}
	
	public void Refresh (int fruitCount, int woodCount) {
		fruit.text = fruitCount.ToString();
		wood.text = woodCount.ToString();
	}
}
