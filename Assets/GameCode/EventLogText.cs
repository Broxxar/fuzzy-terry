﻿using UnityEngine;
using System.Collections;

public class EventLogText : MonoBehaviour {

	TextMesh tm;
	const float fadeDelay = 5;
	const float fadeTime = 3;
	
	public void Set (string s) {
		if (!tm)
			tm = GetComponent<TextMesh>();
		tm.color = Color.white;
		GetComponent<TextMesh>().text = s;
		StartCoroutine(FadeAfterX());
	}
	
	IEnumerator FadeAfterX () {
		yield return new WaitForSeconds (fadeDelay);
		Color fadeColor = tm.color;
		while (tm.color.a > 0) {
			fadeColor.a -= Time.deltaTime/fadeTime;
			tm.color = fadeColor;
			yield return new WaitForEndOfFrame();
		}
		GUICamera.el.CleanUp(this);
	}
}
