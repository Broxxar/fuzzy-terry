﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Terry : MonoBehaviour {

	float hunger = 1f;
	float stamina = 1f;
	int food = 0;
	int wood = 0;

	float walkSpeed = 2;

	bool alive = true;

	int bedLevel = 0;

	const float hungerSleepRate = -0.001f;
	const float hungerAwakeRate = -0.004f;

	const float staminaSleepRate = 0.003f;
	const float staminaAwakeRate = -0.002f;

	Vector3 fruitPos = new Vector3 (9.6f, -3f, 0);
	Vector3 treePos = new Vector3 (-7.6f, -3f, 0);
	Vector3 bedPos = new Vector3 (0, -3f, 0);

	Dictionary<string, Action> Actions = new Dictionary<string, Action>();
	Dictionary<string, float> inputSet = new Dictionary<string, float>();

	Animation anim;

	public Sprite normal;
	public Sprite eating;
	public Sprite sleeping;
	SpriteRenderer terryBody;

	Animation fruitBushes;
	Animation trees;

	ParticleSystem eMark;
	ParticleSystem qMark;
	ParticleSystem zMark;

	enum TerryState {
		Sleeping,
		GatheringFood,
		GatheringWood,
		Idling
	}

	TerryState state = TerryState.Idling;

	void Awake () {

		Actions.Add("Idle", Idle);
		Actions.Add("GatherWood", GatherWood);
		Actions.Add("GatherFood", GatherFood);
		Actions.Add ("EatFood", EatFood);
		Actions.Add ("Sleep", Sleep);

		inputSet.Add("Hunger", 0);
		inputSet.Add("Stamina", 0);
		inputSet.Add("Wood", 0);
		inputSet.Add("Food", 0);
		inputSet.Add("Time", 0);

		FuzzyController.Instance.AddRule (new FuzzyRule(new FuzzyJoin(new FuzzyComplement(new FuzzyTerm("Hunger", "Sated")), new FuzzyComplement(new FuzzyTerm("Food", "None")),"AND"), "EatFood"));
		FuzzyController.Instance.AddRule (new FuzzyRule(new FuzzyJoin(new FuzzyComplement(new FuzzyTerm("Food", "A lot")), new FuzzyTerm("Time", "Morning"), "AND"), "GatherFood"));
		FuzzyController.Instance.AddRule (new FuzzyRule(new FuzzyComplement(new FuzzyJoin(new FuzzyTerm("Wood", "Too much"), new FuzzyTerm("Wood", "A lot"), "OR")), "GatherWood"));
		FuzzyController.Instance.AddRule (new FuzzyRule(new FuzzyJoin(new FuzzyComplement (new FuzzyTerm("Stamina", "Energetic")), new FuzzyTerm("Time", "Nighttime"), "AND"), "Sleep"));
		FuzzyController.Instance.AddRule (new FuzzyRule(new FuzzyTerm("Stamina", "Exhausted"), "Sleep"));

		eMark = transform.FindChild("eMark").particleSystem;
		qMark = transform.FindChild("qMark").particleSystem;
		zMark = transform.FindChild("zMark").particleSystem;

		anim = GetComponentInChildren<Animation>();
		terryBody = anim.GetComponent<SpriteRenderer>();

		fruitBushes = GameObject.Find("fruitBushes").animation;
		trees = GameObject.Find("trees").animation;

	}

	void Start () {
		TerryLogic();
		terryBody.sprite = normal;
	}

	void UpdateInputSet () {
		inputSet["Hunger"] = hunger;
		inputSet["Stamina"] = stamina;
		inputSet["Wood"] = wood;
		inputSet["Food"] = food;
		inputSet["Time"] = WorldState.Instance.time;
	}

	void TerryLogic () {
		UpdateInputSet();
		Dictionary<string, OutputValue> outputs = FuzzyController.Instance.EvaluateRules(inputSet);

		string bestAction = "Idle";
		float bestScore = 0;
		
		foreach (KeyValuePair<string, OutputValue> item in outputs) {
			if (item.Value.count == 0) continue;
			float adjustedScore = Mathf.Pow(item.Value.total, 2) * (item.Value.count/(float)FuzzyController.Instance.Rules.Count);
			if (adjustedScore > bestScore) {

				if (item.Key == "EatFood" && food == 0){
					GUICamera.el.Log("Terry wants to eat but he hasn't got any food.");
					continue;
				}

				bestScore = adjustedScore;
				bestAction = item.Key;
			}
		}

		if (state == TerryState.Sleeping && bestAction != "Sleep" && bestAction != "Idle") {
			Wakeup (Actions[bestAction]);
		}

		else {
			Actions[bestAction].Invoke();
		}
	}

	IEnumerator MoveTo (Vector3 movePos) {
		anim.CrossFade ("walk");

		if (movePos.x < transform.position.x) {
			transform.localScale = new Vector3(-1,1,1);
		}
		else {
			transform.localScale = new Vector3(1,1,1);
		}
		
		while (transform.position != movePos) {
			transform.position = Vector3.MoveTowards(transform.position, movePos, Time.deltaTime * walkSpeed);
			yield return new WaitForEndOfFrame();
		}

		anim.CrossFade ("idle");
	}

	float CalcStamRate () {
		float rate;
		if (state == TerryState.Sleeping) {
			rate = staminaSleepRate + 0.001f * bedLevel;
			if (hunger <= 0.05f)
				rate *= 0.5f;
			if (WorldState.Instance.IsDayTime())
				rate *= 0.5f;
		}

		else {
			rate = staminaAwakeRate;
			if (hunger <= 0.05f)
				rate *= 2;
		}

		return rate;
	}

	float CalcHungerRate () {
		if (state == TerryState.Sleeping) {
			return hungerSleepRate;
		}
		else {
			return hungerAwakeRate;
		}
	}

	void Idle () {
		StartCoroutine(IIdle());
	}

	IEnumerator IIdle () {

		if (state != TerryState.Sleeping) {
			state = TerryState.Idling;
			float randomX = UnityEngine.Random.Range (-3, 3);
			if (randomX > 1 || randomX < -1){
				float randomPos = Mathf.Clamp(transform.position.x + randomX, -4, 4);
				GUICamera.el.Log("Terry wanders aimlessly.");
				yield return StartCoroutine(MoveTo(new Vector3(randomPos, -3, 0)));
			}
			else {
				GUICamera.el.Log("Terry stands around.");
			}
		}
		yield return new WaitForSeconds(2);
		TerryLogic();
	}

	void EatFood () {
		StartCoroutine(IEatFood());
	}

	IEnumerator IEatFood () {
		terryBody.sprite = eating;
		anim.CrossFade ("eat");
		hunger = Mathf.Clamp(hunger + 0.1f, 0, 1);
		food --;
		GUICamera.el.Log("Terry eats some delicious fruit.");
		yield return new WaitForSeconds(4);
		terryBody.sprite = normal;
		anim.CrossFade ("idle");
		yield return new WaitForSeconds(1);
		TerryLogic ();
	}

	void GatherFood () {
		StartCoroutine(IGatherFood());
	}

	IEnumerator IGatherFood () {

		if (state != TerryState.GatheringFood){
			yield return StartCoroutine(MoveTo(fruitPos));
			state = TerryState.GatheringFood;
		}

		fruitBushes.Play("fruitBushBounce");
		yield return new WaitForSeconds(1f);
		fruitBushes.Play("fruitBushBounce");
		yield return new WaitForSeconds(1f);

		if (UnityEngine.Random.Range(0f,1f) >= 0.3f) {
			eMark.Emit (1);
			GUICamera.el.Log("Terry finds some fruit!");
			food ++;
		}

		else {
			GUICamera.el.Log("Terry can't find any fruit.");
			qMark.Emit (1);
		}

		TerryLogic();
	}
	
	void GatherWood () {
		StartCoroutine(IGatherWood());
	}

	IEnumerator IGatherWood () {

		if (state != TerryState.GatheringWood){
			yield return StartCoroutine(MoveTo(treePos));
			state = TerryState.GatheringWood;
		}
		
		trees.Play("treeWiggle");
		yield return new WaitForSeconds(1.1f);
		trees.Play("treeWiggle");
		yield return new WaitForSeconds(1.1f);
		
		if (UnityEngine.Random.Range(0,2) == 0) {
			eMark.Emit (1);
			GUICamera.el.Log("Terry gathers some wood!");
			wood ++;
		}
		
		else {
			GUICamera.el.Log("Terry can't manage to cut down any trees.");
			qMark.Emit (1);
		}

		TerryLogic();
	}

	void Sleep () {
		StartCoroutine(ISleep());
	}

	IEnumerator ISleep () {
		if (state != TerryState.Sleeping) {
			yield return StartCoroutine(MoveTo(bedPos));
			GUICamera.el.Log("Terry goes to sleep.");
			transform.localScale = new Vector3(1,1,1);
			anim.CrossFade("sleep");
			zMark.Play();
			terryBody.sprite = sleeping;
			state = TerryState.Sleeping;
		}
		yield return new WaitForSeconds(5);
		TerryLogic();
	}

	void Wakeup (Action action) {
		StartCoroutine(IWakeup(action));
	}

	IEnumerator IWakeup (Action action) {
		GUICamera.el.Log("Terry wakes up.");
		terryBody.sprite = normal;
		anim.CrossFade("idle");
		state = TerryState.Idling;
		zMark.Stop();
		yield return new WaitForSeconds(0.5f);
		action.Invoke();
	}

	void Update () {
		GUICamera.rc.Refresh(food, wood);
		hunger = Mathf.Clamp(hunger + CalcHungerRate() * Time.deltaTime, 0, 1);
		stamina = Mathf.Clamp(stamina + CalcStamRate() * Time.deltaTime, 0, 1);
	}
}
