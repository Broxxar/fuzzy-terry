﻿using UnityEngine;
using System.Collections;

public class WorldState : MonoBehaviour {

	public static WorldState Instance {get; private set;}

	public float temperature {get; private set;}
	public float time {get; private set;}
	
	public Color nightTimeColor;

	SpriteRenderer[] renderers;
	SpriteRenderer nightSky;
	bool nightTimeRenderers = false;

	public bool IsDayTime () {
		return (time >= 6 && time < 18);
	}

	void Awake () {
		Instance = this;
		time = 6f;
		renderers = GetComponentsInChildren<SpriteRenderer>();
		nightSky = Camera.main.transform.FindChild("nightsky").GetComponent<SpriteRenderer>();
		nightSky.color = Color.clear;
	}

	IEnumerator FadeToDayLight () {

		float t = 0;

		while (t < 10) {
			t = Mathf.Min(t + Time.deltaTime, 10);
			UpdateRenderers(Color.Lerp(nightTimeColor, Color.white, t/10f));
			nightSky.color = Color.Lerp(Color.white, Color.clear, t/10f);
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator FadeToNightTime () { 

		float t = 0;
		
		while (t < 10) {
			t = Mathf.Min(t + Time.deltaTime, 10);
			UpdateRenderers(Color.Lerp(Color.white, nightTimeColor, t/10f));
			nightSky.color = Color.Lerp(Color.clear, Color.white, t/10f);
			yield return new WaitForEndOfFrame();
		}
	}

	void UpdateRenderers (Color color) {
		for (int i = 0; i<renderers.Length; i++) {
			renderers[i].color = color;
		}
	}

	void Update () {
		time += Time.deltaTime * 0.04f;
		if (time > 24)
			time -= 24;

		if (nightTimeRenderers && time >= 6 && time < 18) {
			nightTimeRenderers = false;
			StartCoroutine(FadeToDayLight());
		}

		if (!nightTimeRenderers && time >= 18) {
			Debug.Log("to night time");
			nightTimeRenderers = true;
			StartCoroutine(FadeToNightTime());
		}
	}
}