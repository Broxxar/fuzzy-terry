﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	Transform terry;
	float vel;

	void Awake () {
		terry = GameObject.Find("Terry").transform;
	}

	void Update () {
		float targetX = Mathf.Clamp(terry.position.x, -12.5f + camera.aspect*camera.orthographicSize, 12.5f - camera.aspect*camera.orthographicSize);
		float cameraX = Mathf.SmoothDamp(transform.position.x, targetX, ref vel, 1);
		transform.position = new Vector3(cameraX, 0, -10);
	}
}
