﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventLogger : MonoBehaviour {

	List<EventLogText> textQueue = new List<EventLogText>();

	public void Log (string newEventText) {
		EventLogText tempEvent = ObjectPoolManager.Instance.Pop("EventLogText").GetComponent<EventLogText>();
		tempEvent.gameObject.SetActive(true);
		tempEvent.Set(newEventText);
		textQueue.Add(tempEvent);
		RefreshLog();
	}
	
	public void CleanUp (EventLogText elt) {
		textQueue.Remove(elt);
		ObjectPoolManager.Instance.Push(elt.gameObject);
	}
	
	void RefreshLog () {
		for (int i=0; i < textQueue.Count; i++) {
			float yOffSet = ((textQueue.Count-1) - i) * 0.45f;		
			textQueue[i].transform.parent = transform;
			textQueue[i].transform.localPosition = new Vector3(-0.1f, 0.4f + yOffSet, 0);	
		}
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.A))
			Log ("Herpdeederp derped the flerp.");	
	}
}
