﻿using UnityEngine;
using System.Collections;

public class GUICamera : MonoBehaviour {
	//Use this script to give easy access to all the GUI elements

	public static EventLogger el;
	public static ResourceCount rc;

	void Start() {
		rc = GetComponentInChildren<ResourceCount>();
		el = GetComponentInChildren<EventLogger>();
	}

	void OnGUI () {
		GUI.Box (new Rect(15, 2, 220, 40), "Time Scale");
		Time.timeScale = GUI.HorizontalSlider(new Rect(25, 25, 200, 30), Time.timeScale, 0.0F, 20.0F);
	}
}
