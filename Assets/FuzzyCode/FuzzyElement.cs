﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FuzzyElement {
	public string name;
	public AnimationCurve membershipFunction;
}