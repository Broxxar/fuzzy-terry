﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FuzzyJoin : FuzzyExpression {

	FuzzyExpression leftSide;
	FuzzyExpression rightSide;
	string op;

	public FuzzyJoin (FuzzyExpression ls, FuzzyExpression rs, string _op) {
		leftSide = ls;
		rightSide = rs;
		op = _op;
	}

	public override float Evaluate (Dictionary<string, float> inputSet) {
		if (op == "AND") {
			return Mathf.Min (leftSide.Evaluate(inputSet), rightSide.Evaluate(inputSet));
		}
		else if (op == "OR") {
			return Mathf.Max (leftSide.Evaluate(inputSet), rightSide.Evaluate(inputSet));
		}
		/*
		else if (op == "XOR")
			return 0;

		else if (op == "NAND")
			return 0;
		*/
		else
			return -1; // only reached if an undefined operator is provided
	}
}
