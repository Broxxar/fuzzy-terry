﻿using UnityEngine;
using System.Collections;

public class FuzzyRule {
	public FuzzyExpression fuzzyExpr {get; private set;}
	public string actionName {get; private set;}

	public FuzzyRule(FuzzyExpression expr, string act) {
		fuzzyExpr = expr;
		actionName = act;
	}
}
