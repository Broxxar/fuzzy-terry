﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FuzzyTerm : FuzzyExpression {

	public string setName {get; private set;}
	public string elementName {get; private set;}

	public FuzzyTerm (string sn, string en) {
		setName = sn;
		elementName = en;
	}

	public override float Evaluate (Dictionary<string, float> inputSet) {
		return FuzzyController.Instance.fuzzySets[setName].EvaluateTerm(elementName, inputSet[setName]);
	}
}
