﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FuzzySet : ScriptableObject {
	public List<FuzzyElement> set;

	public float EvaluateTerm (string elementName, float crispValue) {
		for (int i=0; i<set.Count; i++) {
			if (set[i].name == elementName) {
				return set[i].membershipFunction.Evaluate(crispValue);
			}
		}
		return -1;
	}
}