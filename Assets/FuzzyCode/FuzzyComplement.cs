﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FuzzyComplement : FuzzyExpression {

	FuzzyExpression expr;

	public FuzzyComplement (FuzzyExpression _expr) {
		expr = _expr;
	}

	public override float Evaluate (Dictionary<string, float> inputSet) {
		return 1 - expr.Evaluate(inputSet);
	}
}
