﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FuzzyController : MonoBehaviour {

	public static FuzzyController Instance {get; private set;}
	public Dictionary<string, FuzzySet> fuzzySets { get; private set; }

	Dictionary<string, float> inputSet = new Dictionary<string, float>(); // set of crisp values coming in from the world state and terry state
	Dictionary<string, OutputValue> outputSet = new Dictionary<string, OutputValue>();

	public List<FuzzyRule> Rules = new List<FuzzyRule>() ;

	void Awake () {

		Instance = this;

		fuzzySets = new Dictionary<string, FuzzySet>();

		Object[] sets = Resources.LoadAll("FuzzySets");
		for (int i=0; i<sets.Length; i++) {
			fuzzySets.Add(sets[i].name, (FuzzySet)sets[i]);
			inputSet.Add(sets[i].name, 0);
		}

		/* Output values are a tuple of the count (number of times an action has been expressed by a rule)
		 * and the total (sum of rule evaluations applicalbe to this action).
		 * 
		 * This was the hack I talked about in my presentation. Ideally, FuzzyTerms would return a shape
		 * intsead of a 0 to 1 value so that output sets could actually be collections of geometry so that
		 * proper defuzzifcation could be done (although this would lose the idea that actions that have
		 * more rules pointing to them are weighted more than actions with fewer rules.
		 * 
		 * In Terry.cs, the outputSet returned by EvaluateRules is used to caluclate "adjustedScore"s, which
		 * are weighted to determine the best action for Terry at that time.
		 */

		outputSet.Add ("EatFood", new OutputValue (0, 0));
		outputSet.Add ("GatherFood", new OutputValue (0, 0));
		outputSet.Add ("GatherWood", new OutputValue (0, 0));
		outputSet.Add ("Sleep", new OutputValue (0, 0));

	}

	public void AddRule (FuzzyRule newRule) {
		Rules.Add(newRule);
	}

	public void RemoveRule (int index) {
		Rules.RemoveAt(index);
	}

	public Dictionary<string, OutputValue> EvaluateRules (Dictionary<string, float> inputSet) {
		
		foreach (KeyValuePair<string, OutputValue> item in outputSet) {
			item.Value.count = 0;
			item.Value.total = 0;
		}
		
		foreach (FuzzyRule rule in Rules) {
			outputSet[rule.actionName].count ++;
			outputSet[rule.actionName].total += rule.fuzzyExpr.Evaluate(inputSet);
		}

		return outputSet;
	}
}
