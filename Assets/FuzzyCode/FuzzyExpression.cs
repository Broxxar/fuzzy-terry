﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class FuzzyExpression {
	public abstract float Evaluate (Dictionary<string, float> inputSet);
}